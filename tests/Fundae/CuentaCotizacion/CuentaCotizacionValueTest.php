<?php

namespace Fundae\CuentaCotizacion;

use Insidesuki\ValueObject\Fundae\CuentaCotizacion\CuentaCotizacionValue;
use Insidesuki\ValueObject\Fundae\CuentaCotizacion\Exception\EmptyCuentaCotizacionException;
use Insidesuki\ValueObject\Fundae\CuentaCotizacion\Exception\InvalidCuentaCotizacionException;
use PHPUnit\Framework\TestCase;

class CuentaCotizacionValueTest extends TestCase
{


	public function testFailEmpty(){
		$this->expectException(EmptyCuentaCotizacionException::class);
		CuentaCotizacionValue::create('');
	}

	public function testFailInvalidLength(){

		$this->expectException(InvalidCuentaCotizacionException::class);
		CuentaCotizacionValue::create('546565');
	}

	public function testOkValidLengthNine(){
		$cuenta = CuentaCotizacionValue::create('123456789');
		$this->assertTrue($cuenta->isValid);
		$this->assertSame('123456789',$cuenta->cuentaCotizacion());
	}

	public function testOkValidLengthEleven(){
		$cuenta = CuentaCotizacionValue::create('12345678901');
		$this->assertTrue($cuenta->isValid);
	}


	public function testOkAlloEmpty(){
		$cuenta = CuentaCotizacionValue::createAllowEmpty('');
		$this->assertFalse($cuenta->isValid);
	}


	public function testOkOnAllowEmptyAndNotEmpty(){
		$cuenta = CuentaCotizacionValue::createAllowEmpty('12345678901');
		$this->assertTrue($cuenta->isValid);
	}

	public function testTrueEquals(){
		$cuentaUno = CuentaCotizacionValue::createAllowEmpty('12345678901');
		$cuentaDos = CuentaCotizacionValue::create('12345678901');

		$this->assertTrue($cuentaUno->equals($cuentaDos));
	}

	public function testFalseEquals(){
		$cuentaUno = CuentaCotizacionValue::createAllowEmpty('12345678901');
		$cuentaDos = CuentaCotizacionValue::create('123456789');

		$this->assertFalse($cuentaUno->equals($cuentaDos));
	}
}
