<?php

namespace Fundae\CatgoriaProfesional;


use Insidesuki\ValueObject\Fundae\CategoriaProfesional\CategoriaProfesionalValue;
use Insidesuki\ValueObject\Fundae\CategoriaProfesional\Exception\EmptyCategoriaProfesionalException;
use Insidesuki\ValueObject\Fundae\CategoriaProfesional\Exception\InvalidCategoriaProfesionalException;
use PHPUnit\Framework\TestCase;

class CategoriaProfesionalValueTest extends TestCase
{

	public function testFailInvalid(){

		$this->expectException(InvalidCategoriaProfesionalException::class);
		CategoriaProfesionalValue::create('7sss');
	}

	public function testFailEmpty(){

		$this->expectException(EmptyCategoriaProfesionalException::class);
		CategoriaProfesionalValue::create('');
	}


	public function testOkEmpty(){

		$nivel = CategoriaProfesionalValue::createAllowEmpty('');
		$this->assertFalse($nivel->isValid);
		$this->assertEmpty($nivel->categoria());
	}

	public function testOkValid(){

		$nivel = CategoriaProfesionalValue::create('2');
		$this->assertTrue($nivel->isValid);
		$this->assertSame('2',$nivel->categoria());
		$this->assertSame('Mando Intermedio',$nivel->categoriaDesc());


	}

	public function testOkOnCreateWithEmptyButIsNotEmpty(){

		$nivel = CategoriaProfesionalValue::createAllowEmpty('2');
		$this->assertTrue($nivel->isValid);

	}


	public function testEqualsTrue(){

		$nivelA = CategoriaProfesionalValue::create('3');
		$nivelB = CategoriaProfesionalValue::create('3');

		$this->assertTrue($nivelA->equals($nivelB));
	}

	public function testEqualsFalse(){

		$nivelA = CategoriaProfesionalValue::create('3');
		$nivelB = CategoriaProfesionalValue::create('5');

		$this->assertFalse($nivelA->equals($nivelB));
	}




}
