<?php
declare(strict_types = 1);

namespace Fundae\TerrorismoValue;

use Insidesuki\ValueObject\Fundae\TerrorismoValue\Exception\EmptyTerrorismoException;
use Insidesuki\ValueObject\Fundae\TerrorismoValue\Exception\InvalidTerrorismoValueException;
use Insidesuki\ValueObject\Fundae\TerrorismoValue\TerrorismoValue;
use PHPUnit\Framework\TestCase;

class TerrorismoValueTest extends TestCase
{

	public function testFailEmpty()
	{

		$this->expectException(EmptyTerrorismoException::class);
		TerrorismoValue::create('');

	}

	public function testFailInvalid()
	{
		$this->expectException(InvalidTerrorismoValueException::class);
		TerrorismoValue::create('si');

	}

	public function testFailInvalidWhenAllowEmpty()
	{
		$this->expectException(InvalidTerrorismoValueException::class);
		TerrorismoValue::createAllowEmpty('ssi0');
	}

	public function testOkCreate(){

		$Terrorismo = TerrorismoValue::create('0');
		$this->assertTrue($Terrorismo->isValid);
		$this->assertSame('0',$Terrorismo->Terrorismo());
	}

	public function testOkCreateEmpty(){
		$Terrorismo = TerrorismoValue::createAllowEmpty('');
		$this->assertFalse($Terrorismo->isValid);
		$this->assertEmpty($Terrorismo->Terrorismo());
	}

	public function testEqualsTrue(){
		$Terrorismo = TerrorismoValue::create('0');
		$TerrorismoDos = TerrorismoValue::create('0');
		$this->assertTrue($Terrorismo->equals($TerrorismoDos));

	}


	public function testEqualsFalse(){
		$Terrorismo = TerrorismoValue::create('0');
		$TerrorismoDos = TerrorismoValue::create('1');
		$this->assertFalse($Terrorismo->equals($TerrorismoDos));

	}


}
