<?php

namespace Fundae;

use Insidesuki\ValueObject\Fundae\NivelEstudio\Exception\EmptyNivelEstudioException;
use Insidesuki\ValueObject\Fundae\NivelEstudio\Exception\InvalidNivelEstudioException;
use Insidesuki\ValueObject\Fundae\NivelEstudio\NivelEstudioValue;
use PHPUnit\Framework\TestCase;

class NivelEstudioValueTest extends TestCase
{


	public function testFailInvalid(){

		$this->expectException(InvalidNivelEstudioException::class);
		NivelEstudioValue::create('7sss');
	}

	public function testFailEmpty(){

		$this->expectException(EmptyNivelEstudioException::class);
		NivelEstudioValue::create('');
	}


	public function testOkEmpty(){

		$nivel = NivelEstudioValue::createAllowEmpty('');
		$this->assertFalse($nivel->isValid);
		$this->assertEmpty($nivel->nivelEstudio());
	}

	public function testOkValid(){

		$nivel = NivelEstudioValue::create('2');
		$this->assertTrue($nivel->isValid);
		$this->assertSame('2',$nivel->nivelEstudio());
		$this->assertSame('Educación primaria',$nivel->nivelEstudioDesc());


	}

	public function testOkOnCreateWithEmptyButIsNotEmpty(){

		$nivel = NivelEstudioValue::createAllowEmpty('2');
		$this->assertTrue($nivel->isValid);

	}


	public function testEqualsTrue(){

		$nivelA = NivelEstudioValue::create('3');
		$nivelB = NivelEstudioValue::create('3');

		$this->assertTrue($nivelA->equals($nivelB));
	}

	public function testEqualsFalse(){

		$nivelA = NivelEstudioValue::create('3');
		$nivelB = NivelEstudioValue::create('10');

		$this->assertFalse($nivelA->equals($nivelB));
	}







}
