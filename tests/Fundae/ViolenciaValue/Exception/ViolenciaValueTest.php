<?php
declare(strict_types = 1);

namespace Fundae\ViolenciaValue\Exception;

use Insidesuki\ValueObject\Fundae\ViolenciaValue\Exception\EmptyViolenciaException;
use Insidesuki\ValueObject\Fundae\ViolenciaValue\Exception\InvalidViolenciaValueException;
use Insidesuki\ValueObject\Fundae\ViolenciaValue\Exception\ViolenciaValue;
use PHPUnit\Framework\TestCase;

class ViolenciaValueTest extends TestCase
{
	public function testFailEmpty()
	{

		$this->expectException(EmptyViolenciaException::class);
		ViolenciaValue::create('');

	}

	public function testFailInvalid()
	{
		$this->expectException(InvalidViolenciaValueException::class);
		ViolenciaValue::create('si');

	}

	public function testFailInvalidWhenAllowEmpty()
	{
		$this->expectException(InvalidViolenciaValueException::class);
		ViolenciaValue::createAllowEmpty('ssi0');
	}

	public function testOkCreate(){

		$violencia = ViolenciaValue::create('0');
		$this->assertTrue($violencia->isValid);
		$this->assertSame('0',$violencia->violencia());
	}

	public function testOkCreateEmpty(){
		$violencia = ViolenciaValue::createAllowEmpty('');
		$this->assertFalse($violencia->isValid);
		$this->assertEmpty($violencia->violencia());
	}

	public function testEqualsTrue(){
		$violencia = ViolenciaValue::create('0');
		$violenciaDos = ViolenciaValue::create('0');
		$this->assertTrue($violencia->equals($violenciaDos));

	}


	public function testEqualsFalse(){
		$violencia = ViolenciaValue::create('0');
		$violenciaDos = ViolenciaValue::create('1');
		$this->assertFalse($violencia->equals($violenciaDos));

	}

}
