<?php

namespace Fundae\GrupoCotization;

use Insidesuki\ValueObject\Fundae\GrupoCotization\Exception\EmptyGrupoCotizacionException;
use Insidesuki\ValueObject\Fundae\GrupoCotization\Exception\InvalidGrupoCotizacionException;
use Insidesuki\ValueObject\Fundae\GrupoCotization\GrupoCotizacionValue;
use PHPUnit\Framework\TestCase;

class GrupoCotizacionValueTest extends TestCase
{


	public function testFailInvalid(){

		$this->expectException(InvalidGrupoCotizacionException::class);
		GrupoCotizacionValue::create('7sss');
	}

	public function testFailEmpty(){

		$this->expectException(EmptyGrupoCotizacionException::class);
		GrupoCotizacionValue::create('');
	}


	public function testOkEmpty(){

		$nivel = GrupoCotizacionValue::createAllowEmpty('');
		$this->assertFalse($nivel->isValid);
		$this->assertEmpty($nivel->grupoCotizacion());
	}

	public function testOkValid(){

		$nivel = GrupoCotizacionValue::create('2');
		$this->assertTrue($nivel->isValid);
		$this->assertSame('2',$nivel->grupoCotizacion());
		$this->assertSame('Ingenieros técnicos, Peritos y Ayudantes titulados',$nivel->grupoCotizacionDesc());


	}

	public function testOkOnCreateWithEmptyButIsNotEmpty(){

		$nivel = GrupoCotizacionValue::createAllowEmpty('2');
		$this->assertTrue($nivel->isValid);

	}


	public function testEqualsTrue(){

		$nivelA = GrupoCotizacionValue::create('3');
		$nivelB = GrupoCotizacionValue::create('3');

		$this->assertTrue($nivelA->equals($nivelB));
	}

	public function testEqualsFalse(){

		$nivelA = GrupoCotizacionValue::create('3');
		$nivelB = GrupoCotizacionValue::create('5');

		$this->assertFalse($nivelA->equals($nivelB));
	}



}
