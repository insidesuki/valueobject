<?php
declare(strict_types = 1);

namespace Fundae\DiscapacidadValue;

use Insidesuki\ValueObject\Fundae\DiscapacidadValue\DiscapacidadValue;
use Insidesuki\ValueObject\Fundae\DiscapacidadValue\Exception\EmptyDiscapacidadException;
use Insidesuki\ValueObject\Fundae\DiscapacidadValue\Exception\InvalidDiscapacidadValueException;
use PHPUnit\Framework\TestCase;

class DiscapacidadValueTest extends TestCase
{

	public function testFailEmpty()
	{

		$this->expectException(EmptyDiscapacidadException::class);
		DiscapacidadValue::create('');

	}

	public function testFailInvalid()
	{
		$this->expectException(InvalidDiscapacidadValueException::class);
		DiscapacidadValue::create('si');

	}

	public function testFailInvalidWhenAllowEmpty()
	{
		$this->expectException(InvalidDiscapacidadValueException::class);
		DiscapacidadValue::createAllowEmpty('ssi0');
	}

	public function testOkCreate(){

		$discapacidad = DiscapacidadValue::create('0');
		$this->assertTrue($discapacidad->isValid);
		$this->assertSame('0',$discapacidad->discapacidad());
	}

	public function testOkCreateEmpty(){
		$discapacidad = DiscapacidadValue::createAllowEmpty('');
		$this->assertFalse($discapacidad->isValid);
		$this->assertEmpty($discapacidad->discapacidad());
	}

	public function testEqualsTrue(){
		$discapacidad = DiscapacidadValue::create('0');
		$discapacidadDos = DiscapacidadValue::create('0');
		$this->assertTrue($discapacidad->equals($discapacidadDos));

	}


	public function testEqualsFalse(){
		$discapacidad = DiscapacidadValue::create('0');
		$discapacidadDos = DiscapacidadValue::create('1');
		$this->assertFalse($discapacidad->equals($discapacidadDos));

	}
}
