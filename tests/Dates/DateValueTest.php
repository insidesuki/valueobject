<?php
declare(strict_types = 1);

namespace Dates;

use Insidesuki\ValueObject\Dates\DateValue;
use Insidesuki\ValueObject\Dates\Exception\InvalidDateException;
use PHPUnit\Framework\TestCase;


class DateValueTest extends TestCase
{

	public function testOkCrateWithNullDate(){

		$fecha = DateValue::createAllowEmpty('');
		$this->assertFalse($fecha->isValid);

	}

	public function testFailFecha0000(){

		$this->expectException(InvalidDateException::class);
		DateValue::createAllowEmpty('00-00-0000');
	}

	public function testOkSpanishDate():void
	{

		$fecha = DateValue::createAllowEmpty('11-10-1980');
		$this->assertTrue($fecha->isValid);

	}

    public function testDateCreatedOnMonday(): void
    {
        $date = DateValue::createIfNotWeekend('15-01-2024');
        $this->assertInstanceOf(DateValue::class, $date);
    }

    public function testDateNotCreatedOnSunday(): void
    {
        $date = DateValue::createIfNotWeekend('14-01-2024');
        $this->assertNull($date);
    }

    public function testDateNotCreatedOnSaturday(): void
    {
        $date = DateValue::createIfNotWeekend('13-01-2024');
        $this->assertNull($date);
    }


    public function testFalseWhenIsWeekend(): void
    {
        $date = DateValue::create('15-01-2024');
        $this->assertFalse($date->isWeekend());
    }

    public function testTrueWhenIsWeekendOnSunday(): void
    {
        $date = DateValue::create('14-01-2024');
        $this->assertTrue($date->isWeekend());
    }

    public function testTrueWhenIsWeekendOnSaturday(): void
    {
        $date = DateValue::create('13-01-2024');
        $this->assertTrue($date->isWeekend());
    }


    public function testOkAge()
	{
		$currentYear = date('Y');
		$partialDate = date('d-m');
		$date        = $partialDate . '-1980';
		$age = $currentYear - 1980;
		$fecha = DateValue::createAllowEmpty($date);
		$this->assertEquals($age, $fecha->age());

	}
	public function testOkAgeUndefined()
	{

		$fecha = DateValue::createAllowEmpty('');
		$this->assertSame('undefined', $fecha->age());

	}

	public function testFailInvalidDate()
	{
		$this->expectException(InvalidDateException::class);
		DateValue::createAllowEmpty('24-11-00');
	}


    public function testOkEnglishDate()
	{

		$this->expectException(InvalidDateException::class);
		DateValue::createAllowEmpty('1970-12-31');



	}

	public function testEquals(){


		$fecha = DateValue::createAllowEmpty('11-10-1980');
		$fecha2 = DateValue::createAllowEmpty('11-10-1980');
		$this->assertTrue($fecha->equals($fecha2));
		$this->assertTrue($fecha2->equals($fecha));

	}

	public function testNotEquals(){
		$fecha = DateValue::createAllowEmpty('11-10-1980');
		$fecha2 = DateValue::createAllowEmpty('11-10-1982');

		$this->assertFalse($fecha->equals($fecha2));

	}

	public function testCambiarFechaReturnsSame(){

		$fecha = DateValue::createAllowEmpty('11-10-1980');
		$fechaCambiada = $fecha->change('11-10-1980');
		$this->assertSame('11-10-1980',$fecha->dateValue);
		$this->assertTrue($fecha->equals($fechaCambiada));

	}

	public function testCambiarFechaReturnsNew(){

		$fecha = DateValue::createAllowEmpty('11-10-1980');
		$fechaCambiada = $fecha->change('11-10-1981');
		$this->assertSame('11-10-1981',$fechaCambiada->dateValue);
		$this->assertFalse($fecha->equals($fechaCambiada));

	}

	public function testGetDate(){
		$fecha = DateValue::create('11-10-1980');
		$this->assertInstanceOf(\DateTime::class,$fecha->getDate());
	}

	public function testSpanishDate():void{

		$date = DateValue::now();
		$this->assertIsString($date->month());

	}

	public function testAddInterval():void{

		$now = DateValue::now();
		$interval = $now->increment('1 months');

		$this->assertNotSame($interval,$now);


	}


	public function testOkFormat():void{

		$now = DateValue::now();

		$this->assertIsString($now->format('d-m-Y'));

	}

}