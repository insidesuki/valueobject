<?php

declare(strict_types=1);

namespace Dates;

use DateTime;
use Insidesuki\ValueObject\Dates\AvailableDates;
use Insidesuki\ValueObject\Dates\DateValue;
use Insidesuki\ValueObject\Dates\ExcludedDates;
use PHPUnit\Framework\TestCase;

class AvailableDatesTest extends TestCase
{
    private DateTime $baseDate;

    public function setUp(): void
    {
        $this->baseDate = DateTime::createFromFormat('d-m-Y', '15-01-2024');
    }

    public function testOkWithoutWeekendsReturnsWithoutExcludedDates(): void
    {
        $availableDates = (AvailableDates::createWithoutWeekends($this->baseDate))->dateList;

        $januarySunday = DateTime::createFromFormat('d-m-Y', '14-01-2024');
        $januarySaturday = DateTime::createFromFormat('d-m-Y', '13-01-2024');

        $this->assertNotContains($januarySunday, $availableDates);
        $this->assertNotContains($januarySaturday, $availableDates);
        $this->assertCount(23, $availableDates);
    }

    public function testOkWithoutSundayReturnsWithoutExceludedDates(): void
    {
        $availableDates = (AvailableDates::createWithoutSunday($this->baseDate))->dateList;
        $januarySunday = DateValue::create('14-01-2024');

        $this->assertNotContains($januarySunday, $availableDates);

        $this->assertCount(27, $availableDates);
    }

    public function testOkInSpanishFormat(): void
    {
        $availableDates = (AvailableDates::createWithoutSunday($this->baseDate));
        $datesSpanish = $availableDates->inSpanishFormat();
        $this->assertSame('01-01-2024', $datesSpanish[0]);
        $this->assertNotContains('07-01-2024', $datesSpanish);
    }

    public function testOkWithoutWeekendsAndWithExcludedDates(): void
    {
        $date1 = DateTime::createFromFormat('d-m-Y', '15-01-2024');
        $date2 = DateTime::createFromFormat('d-m-Y', '11-01-2024');

        $dates = [$date1, $date2];
        $excludedDates = new ExcludedDates($dates);
        $availableDates = (AvailableDates::createWithoutWeekends($this->baseDate, $excludedDates));

        $this->assertNotContains($date2, $availableDates->dateList);
        $this->assertNotContains($date1, $availableDates->dateList);
        $this->assertCount(21, $availableDates->inSpanishFormat());
    }


    public function testOkWithoutWeekendsAndEmtpyExcludedDates(): void
    {
        $excludedDates = new ExcludedDates();
        $availableDates = (AvailableDates::createWithoutWeekends($this->baseDate, $excludedDates));
        $this->assertCount(23, $availableDates->inSpanishFormat());
    }


}
