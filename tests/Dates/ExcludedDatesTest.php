<?php

declare(strict_types=1);

namespace Dates;

use DateTime;
use Insidesuki\ValueObject\Dates\ExcludedDates;
use PHPUnit\Framework\TestCase;

class ExcludedDatesTest extends TestCase
{

    public function testExcludedDatesWasCreated(): void
    {
        $date1 = new DateTime();
        $date2 = DateTime::createFromFormat('d-m-Y', '16-01-2024');

        $dates = [$date1, $date2];
        $excludedDates = new ExcludedDates($dates);
        $this->assertContains($date1, $excludedDates->__toArray());
    }

    public function testExcludedDatesWasReturnEmptyArray(): void
    {

        $excludedDates = new ExcludedDates();
        $this->assertEmpty($excludedDates->inSpanishFormat());
    }

}
