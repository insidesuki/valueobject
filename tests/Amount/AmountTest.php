<?php

declare(strict_types=1);

namespace Amount;

use Insidesuki\ValueObject\Amount\Amount;
use Insidesuki\ValueObject\Amount\Exception\InvalidAmountValue;
use PHPUnit\Framework\TestCase;

class AmountTest extends TestCase
{

	public function testOkAmountWasCreated(): void
	{

		$testValue = 123;
		$amount = Amount::create($testValue);
		$this->assertEquals($testValue, $amount->amount);

	}

	public function testFailAmountHasInvalidValue(): void
	{

		$this->expectException(InvalidAmountValue::class);
		$testValue = -1123;
		Amount::create($testValue);

	}

	public function testOkAmountHasNegativeValue(): void
	{
		$testValue = -1123;
		$amount = Amount::create($testValue,true);
		$this->assertSame($testValue,$amount->amount);

	}

	public function testOkEquals():void{

		$firstAmount = Amount::create(123);
		$secondAmount = Amount::create(123);

		$this->assertTrue($firstAmount->equals($secondAmount));
		$this->assertTrue($secondAmount->equals($firstAmount));


	}

	public function testFailEquals():void{

		$firstAmount = Amount::create(123);
		$secondAmount = Amount::create(124);

		$this->assertFalse($firstAmount->equals($secondAmount));
		$this->assertFalse($secondAmount->equals($firstAmount));


	}

	public function testOkIsImmutableWhenSameValueOfChange():void{


		$amount = Amount::create(123);
		$amount2 = $amount->change(123);

		$this->assertSame($amount,$amount2);


	}


	public function testOkIsImmutableWhenDifferentValueOfChange():void{


		$amount = Amount::create(123);
		$amount2 = $amount->change(124);
		$this->assertNotSame($amount,$amount2);


	}

	public function testOkReturnsFloat(): void
	{

		$amount = Amount::create(123);
		$this->assertSame(1.23, $amount->toFloat());

	}

	public function testOkReturnsStringSpanishFormat(): void
	{

		$amount = Amount::create(123);
		$this->assertSame('1,23', $amount->toSpanishFormat());


	}

	public function testOkCreateFromInput(): void
	{

		$amount = Amount::createFromSpanishFormat('250');
		$this->assertSame(250.00, $amount->toFloat());
		$this->assertSame('250,00€',$amount->toEuro());

	}


	public function testOkCreateFromSpanishFormat(): void
	{

		$value = '1,23';
		$amount = Amount::createFromString($value);
		$this->assertSame(123, $amount->amount);


	}

	public function testOkCreateFromSpanishFormatWithThousandSeparator(): void
	{

		$value = '1.023,12';
		$amount = Amount::createFromString($value);
		$this->assertSame(102312, $amount->amount);


	}


	public function testOkReturnToEuro(): void
	{

		$amount = Amount::create(123);
		$this->assertSame('1,23€', $amount->toEuro());

	}

	public function testOkWhenIncrement(): void
	{

		$value1 = 123;
		$increment = 100;

		$incrementedValue = $value1 + $increment;

		$amount = Amount::create($value1);
		$newAmount = $amount->increment($increment);

		$this->assertNotSame($newAmount, $amount);
		$this->assertSame($incrementedValue, $newAmount->amount);


	}


	public function testFailWhenDecrease(): void
	{

		$this->expectException(\DomainException::class);

		$value1 = 123;
		$decrease = 124;

		$amount = Amount::create($value1);
		$amount->decrease($decrease);


	}

	public function testOkWhenDecrease(): void
	{

		$value1 = 123;
		$decrease = 100;

		$decreasedValue = $value1 - $decrease;

		$amount = Amount::create($value1);
		$newAmount = $amount->decrease($decrease);

		$this->assertNotSame($newAmount, $amount);
		$this->assertSame($decreasedValue, $newAmount->amount);



	}

	public function testOkWhenDecreaseReturnsZero(): void
	{

		$value1 = 123;
		$decrease = 123;


		$amount = Amount::create($value1);
		$newAmount = $amount->decrease($decrease);

		$this->assertSame(0, $newAmount->amount);


	}


	public function testOkAmountWasCreatedFromFloatWithOneDecimals():void{

		$float = 142.4;
		$amount = Amount::createFromFloat($float);
		$this->assertSame(1424,$amount->amount);


	}

	public function testOkAmountWasCreatedFromFloatWithTwoDecimals():void{

		$float = 11232132.45;
		$amount = Amount::createFromFloat($float);
		$this->assertSame(1123213245,$amount->amount);

	}
	public function testOkAmountWasCreatedFromFloatWithThreeDecimals():void{

		$float = 142.455;
		$amount = Amount::createFromFloat($float);
		$this->assertSame(142455,$amount->amount);


	}

	public function testOkAmountWasCreatedFromFloatWithFourDecimals():void{

		$float = 14222.1234;
		$amount = Amount::createFromFloat($float);
		$this->assertSame(142221234,$amount->amount);
		$this->assertSame($float,$amount->toFloat(10000));


	}


}
