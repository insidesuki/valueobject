<?php

namespace String\Email;

use Insidesuki\ValueObject\String\Email\EmailValue;
use Insidesuki\ValueObject\String\Email\Exception\InvalidEmailException;
use PHPUnit\Framework\TestCase;

class EmailValueTest extends TestCase
{


	public function testFailInvalidEmail(){

		$email = 'sdsdsd@anuñnió.com';
		$this->expectException(InvalidEmailException::class);
		EmailValue::create($email);

	}

	public function testFailInvalidEmptyEmail(){

		$email = '';
		$this->expectException(InvalidEmailException::class);
		EmailValue::create($email);

	}

	public function testOkValidEmail(){

		$emailString  = '  info@none.es';
		$email = EmailValue::create($emailString);
		$this->assertTrue($email->isValid);
		$this->assertSame($email->emailAddress,$emailString);

	}

	public function testOkEmptyEmail(){

		$emailString = '';
		$email = EmailValue::createAllowEmpty($emailString);
		$this->assertFalse($email->isValid);
		$this->assertEmpty($email->emailAddress);
	}

	public function testFailEmptyInvalid(){

		$this->expectException(InvalidEmailException::class);
		$emailString = 'asdsad';
		EmailValue::createAllowEmpty($emailString);

	}


	public function testEqualsOk(){
		$emailString  = 'info@none.es';
		$email1 = EmailValue::create($emailString);
		$email2 = EmailValue::create($emailString);

		$this->assertTrue($email1->equals($email2));

	}

	public function testEqualsfail(){
		$emailString  = 'info@none.es';
		$email1 = EmailValue::create($emailString);
		$email2 = EmailValue::create('info2@nn.es');

		$this->assertFalse($email1->equals($email2));

	}




}
