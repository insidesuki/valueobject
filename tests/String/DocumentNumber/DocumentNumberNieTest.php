<?php
declare(strict_types = 1);

namespace String\DocumentNumber;

use Insidesuki\ValueObject\String\DocumentNumber\DocumentNumber;
use Insidesuki\ValueObject\String\DocumentNumber\Exception\InvalidDocumentNumberException;
use PHPUnit\Framework\TestCase;

class DocumentNumberNieTest extends TestCase
{
	public function testFailInvalidNieSendingAnNif(){

		$nieToCreate = '13965664h';
		$this->expectException(InvalidDocumentNumberException::class);
		$this->expectExceptionMessage(sprintf('The DocumentNumber: %s, with type: NIE has invalid',strtoupper
		($nieToCreate)));
		$nie = DocumentNumber::createByType('60',$nieToCreate);


	}

	public function testFailInvalidNieSendingAnCif(){

		$nieToCreate = 'Q2559024A';
		$this->expectException(InvalidDocumentNumberException::class);
		$this->expectExceptionMessage(sprintf('The DocumentNumber: %s, with type: NIE has invalid',strtoupper($nieToCreate)));
		DocumentNumber::createByType('60',$nieToCreate);


	}


	public function testFailInvalidNieSendingAnInvalidNum(){

		$nieToCreate = '139sdsd65664h';
		$this->expectException(InvalidDocumentNumberException::class);
		$this->expectExceptionMessage(sprintf('The DocumentNumber: %s, with type: NIE has invalid',strtoupper($nieToCreate)));
		$nie = DocumentNumber::createByType('60',$nieToCreate);


	}


	public function testOkNieValid(){
		$num = 'Z3740746p';
		$nie = DocumentNumber::createByType('60',$num);
		$this->assertInstanceOf(DocumentNumber::class,$nie);
		$this->assertSame(strtoupper($num),$nie->numDocument());
		$this->assertTrue($nie->isValid);
		$this->assertSame('60',$nie->type());
		$this->assertSame('nie',$nie->descDocumentType());


	}

}
