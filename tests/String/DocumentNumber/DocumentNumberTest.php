<?php
declare(strict_types = 1);

namespace String\DocumentNumber;

use Insidesuki\ValueObject\String\DocumentNumber\DocumentNumber;
use Insidesuki\ValueObject\String\DocumentNumber\Exception\InvalidDocumentNumberException;
use PHPUnit\Framework\TestCase;

class DocumentNumberTest extends TestCase
{

	public function testOkCreateOnInvalid(){

		$num = DocumentNumber::create('123213',false);
		$this->assertInstanceOf(DocumentNumber::class,$num);
		$this->assertFalse($num->isValid);
		$this->assertSame('20',$num->documentType()->value);
		$this->assertSame('passport',$num->documentType()->name);

	}

	public function testFailInvalidDocument(){

		$this->expectException(InvalidDocumentNumberException::class);
		$num = DocumentNumber::create('98798');
	}

	public function testOkValidNif(){

		$nifToCreate = '13965664h';
		$nif = DocumentNumber::create($nifToCreate);
		$this->assertInstanceOf(DocumentNumber::class,$nif);
		$this->assertSame('10',$nif->documentType()->value);
		$this->assertSame('nif',$nif->documentType()->name);
		$this->assertTrue($nif->isValid);

	}

	public function testOkValidNie(){

		$nieToCreate = 'Z3740746p';
		$nie = DocumentNumber::create($nieToCreate);
		$this->assertInstanceOf(DocumentNumber::class,$nie);
		$this->assertSame('60',$nie->documentType()->value);
		$this->assertSame('nie',$nie->documentType()->name);
		$this->assertTrue($nie->isValid);

	}

	public function testOkValidCif(){

		$cifToCreate = 'Q2559024A';
		$cif = DocumentNumber::create($cifToCreate);
		$this->assertInstanceOf(DocumentNumber::class,$cif);
		$this->assertSame('90',$cif->documentType()->value);
		$this->assertSame('cif',$cif->documentType()->name);
		$this->assertTrue($cif->isValid);

	}


	public function testEquals(){

		$cifToCreate = 'Q2559024A';
		$cif = DocumentNumber::create($cifToCreate);
		$cif2 = DocumentNumber::create($cifToCreate);
		$this->assertTrue($cif->equals($cif2));


	}

	public function testChangeReturnsSame():void{

		$cifToCreate = 'Q2559024A';
		$cif = DocumentNumber::create($cifToCreate);
		$cifChanged = $cif->change('90',$cifToCreate);
		$this->assertTrue($cif->equals($cifChanged));

	}

	public function testChangeReturnsNewInstance():void{

		$cifToCreate = 'X0010863F';
		$cif = DocumentNumber::create($cifToCreate);
		$cifChanged = $cif->change('60','Z2501302B');
		$this->assertFalse($cif->equals($cifChanged));

	}

}
