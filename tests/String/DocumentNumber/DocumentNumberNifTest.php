<?php
declare(strict_types = 1);

namespace String\DocumentNumber;

use Insidesuki\ValueObject\String\DocumentNumber\DocumentNumber;
use Insidesuki\ValueObject\String\DocumentNumber\Exception\InvalidDocumentNumberException;
use PHPUnit\Framework\TestCase;

class DocumentNumberNifTest extends TestCase
{

	public function testFailInvalidNif(){

		$num = 'sdsdsd';
		$this->expectException(InvalidDocumentNumberException::class);
		$this->expectExceptionMessage(sprintf('The DocumentNumber: %s, with type: NIF has invalid',strtoupper($num)));
		DocumentNumber::createByType('10',$num);

	}

	public function testFailInvalidNifWhenSendNie(){

		$num = 'Z3740746P';
		$this->expectException(InvalidDocumentNumberException::class);
		$this->expectExceptionMessage(sprintf('The DocumentNumber: %s, with type: NIF has invalid',strtoupper($num)));
		$nif = DocumentNumber::createByType('10',$num);

	}

	public function testFailInvalidNifWhenSendCif(){

		$num = 'Q2559024A';
		$this->expectException(InvalidDocumentNumberException::class);
		$this->expectExceptionMessage(sprintf('The DocumentNumber: %s, with type: NIF has invalid',strtoupper($num)));
		$nif = DocumentNumber::createByType('10',$num);

	}

	public function testOkValidNif(){

		$nifToCreate = '13965664h';
		$nif = DocumentNumber::createByType('10',$nifToCreate);
		$this->assertInstanceOf(DocumentNumber::class,$nif);
		$this->assertTrue($nif->isValid);
		$this->assertSame(strtoupper($nifToCreate),$nif->numDocument());
		$this->assertSame('10',$nif->type());
		$this->assertSame('nif',$nif->descDocumentType());
	}


}
