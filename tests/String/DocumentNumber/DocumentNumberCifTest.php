<?php
declare(strict_types = 1);

namespace String\DocumentNumber;

use Insidesuki\ValueObject\String\DocumentNumber\DocumentNumber;
use Insidesuki\ValueObject\String\DocumentNumber\Exception\InvalidDocumentNumberException;
use Insidesuki\ValueObject\String\DocumentNumber\Exception\InvalidDocumentTypeException;
use PHPUnit\Framework\TestCase;

class DocumentNumberCifTest extends TestCase
{

	public function testFailInvalidTypeString(){


		$this->expectException(InvalidDocumentTypeException::class);
		$numDocument = DocumentNumber::createByType('123','89789nsad');

	}


	public function testFailInvalidCifWhenSendNie(){

		$num = 'Z3740746P';
		$this->expectException(InvalidDocumentNumberException::class);
		$this->expectExceptionMessage(sprintf('The DocumentNumber: %s, with type: CIF has invalid',strtoupper($num)));
		$nif = DocumentNumber::createByType('90',$num);

	}
	public function testFailInvalidCifWhenSendNif(){

		$num = '13965664h';
		$this->expectException(InvalidDocumentNumberException::class);
		$this->expectExceptionMessage(sprintf('The DocumentNumber: %s, with type: CIF has invalid',strtoupper($num)));
		$nif = DocumentNumber::createByType('90',$num);

	}

	public function testFailInvalidCifWhenSendInvalidNum(){

		$num = '13965asd6saasd64h';
		$this->expectException(InvalidDocumentNumberException::class);
		$this->expectExceptionMessage(sprintf('The DocumentNumber: %s, with type: CIF has invalid',strtoupper($num)));
		$nif = DocumentNumber::createByType('90',$num);

	}

	public function testOkCifValid(){
		$num = 'Q2559024A';
		$cif = DocumentNumber::createByType('90',$num);
		$this->assertInstanceOf(DocumentNumber::class,$cif);
		$this->assertSame(strtoupper($num),$cif->numDocument());
		$this->assertTrue($cif->isValid);
		$this->assertSame('90',$cif->type());
		$this->assertSame('cif',$cif->descDocumentType());


	}

}
