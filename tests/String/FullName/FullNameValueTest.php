<?php

namespace String\FullName;

use Insidesuki\ValueObject\String\FullName\Exception\InvalidDataFullName;
use Insidesuki\ValueObject\String\FullName\FullNameValue;
use PHPUnit\Framework\TestCase;

class FullNameValueTest extends TestCase
{


	public function testFailEmptyName(){

		$this->expectException(InvalidDataFullName::class);
		$this->expectExceptionMessage('Name is required');
		FullNameValue::create('','asd');

	}

	public function testFailEmptyLastname(){

		$this->expectException(InvalidDataFullName::class);
		$this->expectExceptionMessage('Lastname1 is required');
		FullNameValue::create('asdsd','');

	}

	public function testOkWithNameAndLastname1():void{

		$name = 'james';
		$lastname = 'bond';
		$fullName = FullNameValue::create($name,$lastname);
		$this->assertSame($name,$fullName->name());
		$this->assertSame($lastname,$fullName->lastname1());
		$this->assertEmpty($fullName->lastname2());

	}

	public function testOkFull():void{

		$name = 'james';
		$lastname = 'bond';
		$lastname1 = 'Fox';
		$fullName = FullNameValue::create($name,$lastname,$lastname1);
		$this->assertSame($lastname1,$fullName->lastname2());

	}

	public function testOkTrims():void{
		$name = '  james  ';
		$lastname = '    bond  ';
		$lastname1 = '  Fox    ';
		$fullName = FullNameValue::create($name,$lastname,$lastname1);
		$this->assertSame('james',$fullName->name());
		$this->assertSame('bond',$fullName->lastname1());
		$this->assertSame('Fox',$fullName->lastname2());
	}


	public function testOkFullName(){

		$name = '  james  ';
		$lastname = '    bond  ';
		$lastname1 = '  Fox    ';
		$fullName = FullNameValue::create($name,$lastname,$lastname1);
		$this->assertSame('james bond Fox',$fullName->fullName());


	}

	public function testOkFullNameWithoutLastname2(){

		$name = '  james  ';
		$lastname = '    bond  ';
		$fullName = FullNameValue::create($name,$lastname);
		$this->assertSame('james bond',$fullName->fullName());


	}

	public function testOkFullNameByLastname(){

		$name = '  james  ';
		$lastname = '    bond  ';
		$lastname1 = '  Fox    ';
		$fullName = FullNameValue::create($name,$lastname,$lastname1);
		$this->assertSame('bond Fox, james',$fullName->fullNameByLastname());


	}

	public function testOkFullNameByLastnameWithoutLastname2(){

		$name = '  james  ';
		$lastname = '    bond  ';
		$fullName = FullNameValue::create($name,$lastname);
		$this->assertSame('bond, james',$fullName->fullNameByLastname());


	}

	public function testOkEquals(){

		$name = '  james  ';
		$lastname = '    bond  ';
		$fullName = FullNameValue::create($name,$lastname);
		$fullName2 = FullNameValue::create($name,$lastname);
		$this->assertTrue($fullName->equals($fullName2));


	}

	public function testFalseEquals(){

		$name = '  james  ';
		$lastname = '    bond  ';
		$fullName = FullNameValue::create($name,$lastname);
		$fullName2 = FullNameValue::create($name,'Bond2');
		$this->assertFalse($fullName->equals($fullName2));


	}

	public function testChangeReturnsSame(){
		$name = '  james  ';
		$lastname = '    bond  ';
		$fullName = FullNameValue::create($name,$lastname);
		$fullName2 = $fullName->change('james','bond');
		$this->assertTrue($fullName->equals($fullName2));

	}

	public function testChangeReturnsNewInstance(){
		$name = '  james  ';
		$lastname = '    bond  ';
		$fullName = FullNameValue::create($name,$lastname);
		$fullName2 = $fullName->change('james','bond','yuyu');
		$this->assertFalse($fullName->equals($fullName2));

	}



}
