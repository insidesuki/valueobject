<?php

namespace String;

use Insidesuki\ValueObject\String\StringValue;
use PHPUnit\Framework\TestCase;

class StringValueTest extends TestCase
{

	public function testOnlyNumbers()
	{
		$num = 'j.hj.k8.768n  97.98.77 * .kl .. *';
		$string = new StringValue($num);
		$this->assertEquals('8768979877', $string->onlyNumbers());
	}

	public function testRemoveSpaces()
	{
		$num = '  k i  s        s   ';
		$string = new StringValue($num);
		$this->assertEquals('kiss', $string->removeSpaces());
	}


	public function testCleanUp()
	{
		$num = ' d.l &/l 09 !2 ';
		$string = new StringValue($num);
		$this->assertEquals('dll092', $string->clean());
	}

	public function testSuccessStartsWith()
	{

		$num = '99911099911';
		$string = new StringValue($num);
		$this->assertTrue($string->startsWith(['9','7']));
	}

	public function testSuccessStartsWithOnlyOneElement()
	{

		$num = '99911099911';
		$string = new StringValue($num);
		$this->assertTrue($string->startsWith(['9']));
	}



	public function testFailStarsWith()
	{
		$num = '199911099911';
		$string = new StringValue($num);
		$this->assertFalse($string->startsWith(['9', '7']));

	}

	public function testFailEmptyString()
	{
		$txt = '';
		$string = new StringValue($txt);
		$this->assertFalse($string->isFilled());
	}


	public function testOnlyLetters()
	{

		$txt = '21321jkjkjk';
		$string = new StringValue($txt);
		$string->onlyLetters();
		$this->assertSame('jkjkjk', $string->__toString());

	}


	public function testIsOnlyLetters()
	{
		$txt = 'kjkljkljkljkl oioio';
		$string = new StringValue($txt);
		$this->assertTrue($string->isOnlyLetters());
	}


	public function testFalseIsOnlyLetters()
	{
		$txt = 'kjkljkljkljkl3213213 oioio';
		$string = new StringValue($txt);
		$this->assertFalse($string->isOnlyLetters());
	}

	public function testValidLength():void{

		$txt = 'uno';
		$string = new StringValue($txt);
		$this->assertTrue($string->validLength(3));


	}


	public function testLength():void{

		$txt = 'uno';
		$string = new StringValue($txt);
		$this->assertSame(3,$string->length());


	}

	public function testToUpper():void{

		$txt = 'uno';
		$string = new StringValue($txt);
		$string->toUpper();
		$this->assertSame('UNO',$string->__toString());


	}

	public function testToTrim():void{

		$txt = ' uno  ';
		$string = new StringValue($txt);
		$string->toTrim();
		$this->assertSame('uno',$string->__toString());


	}


}
