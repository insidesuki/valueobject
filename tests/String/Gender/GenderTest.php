<?php

namespace String\Gender;

use Insidesuki\ValueObject\String\Gender\Exception\InvalidGenderTypeException;
use Insidesuki\ValueObject\String\Gender\Gender;
use Insidesuki\ValueObject\String\Gender\GenderType;
use PHPUnit\Framework\TestCase;

class GenderTest extends TestCase
{

	public function testFailInvalidGender():void{

		$this->expectException(InvalidGenderTypeException::class);
		Gender::create('asdsad');

	}

	public function testOkUndefinded():void{

		$gender = Gender::create('');
		$this->assertFalse($gender->isValid);
		$this->assertSame(GenderType::Undefined,$gender->genderType);
		$this->assertSame(GenderType::Undefined->value,$gender->genderType->value);

	}

	public function testOkMale():void
	{
		$gender = Gender::create('1');
		$this->assertTrue($gender->isValid);
		$this->assertSame(GenderType::Masculino,$gender->genderType);
		$this->assertSame('1',$gender->genderType->value);
		$this->assertSame('1',$gender->gender());

	}

	public function testOkFemale():void{
		$gender = Gender::create('2');
		$this->assertTrue($gender->isValid);
		$this->assertSame(GenderType::Femenino,$gender->genderType);
		$this->assertSame('2',$gender->genderType->value);

	}

	public function testEquals():void{

		$gender = Gender::create('2');
		$gender2 = Gender::create('2');
		$this->assertTrue($gender->equals($gender2));

	}

	public function testFalseEquals():void{

		$gender = Gender::create('2');
		$gender2 = Gender::create('1');
		$this->assertFalse($gender->equals($gender2));

	}


	public function testEqualsByString():void{

		$gender = Gender::create('2');
		$gender2 = Gender::create('2');
		$this->assertTrue($gender->equalsByValue('2'));

	}



	public function testChangeReturnsSameInstance():void{

		$gender = Gender::create('2');
		$genderMmod = $gender->change('2');
		$this->assertTrue($gender->equals($genderMmod));



	}

	public function testChangeReturnsNewInstance():void{

		$gender = Gender::create('');
		$genderMmod = $gender->change('2');
		$this->assertFalse($gender->equals($genderMmod));



	}





}
