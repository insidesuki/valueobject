<?php

namespace String\Phone;

use Insidesuki\ValueObject\String\Phone\Exception\EmptyPhoneNumberException;
use Insidesuki\ValueObject\String\Phone\Exception\InvalidPhoneNumberException;
use Insidesuki\ValueObject\String\Phone\PhoneValue;
use PHPUnit\Framework\TestCase;

class PhoneValueTest extends TestCase
{

	public function testFailEmptyPhone()
	{

		$this->expectException(EmptyPhoneNumberException::class);
		$phone = PhoneValue::createPhone('');
	}


	public function testFailEmptyMovilPhone()
	{

		$this->expectException(EmptyPhoneNumberException::class);
		$phone = PhoneValue::createMovilPhone('');
	}

	public function testOkEmptyPhone()
	{
		$phone = PhoneValue::createPhoneAllowEmpty('');
		$this->assertFalse($phone->isValid);
	}

	public function testOkNotEmptyPhone()
	{
		$phone = PhoneValue::createAllowEmpty('915678425');
		$this->assertTrue($phone->isValid);
	}



	public function testFailInvalidNumberMoreThanNine()
	{

		$this->expectException(InvalidPhoneNumberException::class);
		PhoneValue::createPhone('6564654564654654654');
	}

	public function testFailInvalidMovilNumberMoreThanNine()
	{

		$this->expectException(InvalidPhoneNumberException::class);
		PhoneValue::createMovilPhone('6564654564654654654');
	}

	public function testOkNumberPhone()
	{

		$phone = PhoneValue::createPhone('915655310');
		$this->assertTrue($phone->isValid);
		$this->assertSame('915655310', $phone->phoneNumber());
	}

	public function testOkMovilPhoneWithSix()
	{

		$phone = PhoneValue::createMovilPhone('635475297');
		$this->assertTrue($phone->isValid);
		$this->assertSame('635475297', $phone->phoneNumber());
	}

	public function testOkMovilPhoneWithSeven()
	{

		$phone = PhoneValue::createMovilPhone('735475297');
		$this->assertTrue($phone->isValid);
		$this->assertSame('735475297', $phone->phoneNumber());
	}

	public function testOkNumberPhoneWithSpaces()
	{

		$phone = PhoneValue::createPhone(' 915 6553 1 0 ');
		$this->assertTrue($phone->isValid);
		$this->assertSame('915655310', $phone->phoneNumber());
	}

	public function testOkMovilPhoneWithSpaces()
	{

		$phone = PhoneValue::createMovilPhone(' 73 547529 7 ');
		$this->assertTrue($phone->isValid);
		$this->assertSame('735475297', $phone->phoneNumber());
	}


	public function testOkNumberPhoneWithLetters()
	{

		$phone = PhoneValue::createPhone(' 915€sd6553 1 0 ');
		$this->assertTrue($phone->isValid);
		$this->assertSame('915655310', $phone->phoneNumber());
	}

	public function testOkMovilePhoneWithLetters()
	{

		$phone = PhoneValue::createMovilPhone(' h73 5475ey29 7 ');
		$this->assertTrue($phone->isValid);
		$this->assertSame('735475297', $phone->phoneNumber());
	}

	public function testFailNumberPhoneNoStartsWithNine()
	{

		$this->expectException(InvalidPhoneNumberException::class);
		PhoneValue::createPhone('z871123567');

	}

	public function testFailNumberMovilePhoneNoStartsWithSix()
	{

		$this->expectException(InvalidPhoneNumberException::class);
		PhoneValue::createMovilPhone('z871123567');

	}

	public function testFailNumberMovilePhoneNoStartsWithSeven()
	{

		$this->expectException(InvalidPhoneNumberException::class);
		PhoneValue::createMovilPhone('z871123567');

	}

	public function testFailNumberPhoneInvalidWhenAllowsEmpty()
	{

		$this->expectException(EmptyPhoneNumberException::class);
		PhoneValue::createPhone('');

	}

	public function testFailMovilPhoneInvalidWhenAllowsEmpty()
	{

		$this->expectException(EmptyPhoneNumberException::class);
		PhoneValue::createMovilPhone('');

	}

	public function testFalseEquals()
	{

		$movil  = PhoneValue::createMovilPhone('698452478');
		$movil2 = PhoneValue::createMovilPhone('618442478');
		$this->assertFalse($movil->equals($movil2));

	}

	public function testOkEquals()
	{

		$movil  = PhoneValue::createMovilPhone('698452478');
		$movil2 = PhoneValue::createMovilPhone('698452478');
		$this->assertTrue($movil->equals($movil2));

	}



	public function testCreateOkPhoneWithAlloEmptyWhenNotEmpty()
	{

		$phone = PhoneValue::createPhoneAllowEmpty('915789456');
		$this->assertTrue($phone->isValid);

	}

	public function testFailWhenCreate(){

		$this->expectException(EmptyPhoneNumberException::class);
		PhoneValue::create('');

	}

	public function testOkWhenCreateMobile(){

		$mobile = PhoneValue::create('612789159');
		$this->assertTrue($mobile->isValid);
		$this->assertSame('612789159',$mobile->phoneNumber());

	}


	public function testOkWhenCreatePhone(){

		$mobile = PhoneValue::create('912789159');
		$this->assertTrue($mobile->isValid);
		$this->assertSame('912789159',$mobile->phoneNumber());

	}

	public function testFailWhenCreatePhone(){

		$this->expectException(InvalidPhoneNumberException::class);
		$mobile = PhoneValue::create('812789159');

	}


}
