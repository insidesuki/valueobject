**VALUE-OBJECTS**
==============================

---

Common ValueObjects.
Complete doc: https://gitlab.com/insidesuki/valueobject

**1- DateValue**
--
Simple ValueObject to manage DateTime types.

- To create a new DateValue :
```
$startData = DateValue::create('31-10-2010');

```
- Exception on invalid date
> InvalidDateException

- Get Age
```
$date = DateValue::create('31-10-2010');
echo $date->age();



```

**2- DocumentNumber**
--

Spanish NIF/CIF/NIF. Support for Passport type(no validation available)

- Supported types:
````
enum DocumentType: string
{
	case nif = '10';
	case passport = '20';
	case nie = '60';
	case cif = '90';

}

````
- To create a new DocumentNumber by DocumentType
```
// nif
$nif = DocumentNumber::createByType('90','Z3740746P');


````

> On invalid number throws InvalidDocumentNumberException
> 
> On invalid documentType throws InvalidDocumentTypeException

- To create a new DocumentNumber without specify type

```
// number, documentType detected on execution time
$number = DocumentNumber::create('Z3740746P');

// to avoid validation, passport type returned
$numberWithoutValidatin = DocumentNumber::create('posduhsui',false);

````

- To change DocumentNumber with specify type

```

$nif->change('60','newNumber');


````

**3- FullName**
--

Name, lastname1,lastname2
Lastname2 is optional

```

// lastname2 is optional
$name = '  james  ';
$lastname = '    bond  ';
$lastname1 = ' ';
$fullName = FullNameValue::create($name,$lastname,$lastname1);

```



**4- Gender**
--

Generic Gender, admits empty string 

- GenderTypes

```

enum GenderType:string
{

	case Masculino = '1';
	case Femenino = '2';
	case Undefined = '';

}


```
- To create
```
// Female
Gender::create('2');
// Male
Gender::create('1');
// Undefined
Gender::create('');

```
- Exceptions
> InvalidGenderTypeException

**5- EmailValue**
--

Email value object, admits empty string 


```
- To create
```
```
// valid
$email = EmailValue::create($emailString);
// empty
$email = EmailValue::createAllowEmpty(''');

```

- Exceptions
> InvalidEmailException


**6- PhoneNumberValue**
--

PhoneNumber value object, admits empty string


```
- To create
```
```
// valid
$phone = PhoneValue::createPhone('987154362');
// empty
$phoneEmpty = PhoneValue::createPhoneAllowEmpty('');

// movil
$movil = PhoneValue::createMovilPhone('714652391');
// empty
$movil = PhoneValue::createMovilPhoneAllowEmpty('714652391');

```

- Exceptions
> InvalidPhoneNumberException
> EmptyPhoneNumberException

**7- NivelEstudioValue**
--

FUNDAE nivel estudios

**8- CategoriaProfesionalValue**
--

FUNDAE categoria profesional.

**9- GrupoCotizacionValue**
--

FUNDAE grupo cotizacion

**10- CuentaCotizacionValue**
--

FUNDAE cuenta cotizacion

**11- TerrorismoValue**
--

FUNDAE afectados terrorismo.

**12- ViolenciaValue**
--

FUNDAE afectados violencia de genero

**14- DiscapacidadValue**
--

FUNDAE afectados discapacidad.

**15- Amount**
--

