<?php
declare(strict_types=1);

namespace Insidesuki\ValueObject\Amount\Exception;

class InvalidAmountValue extends \DomainException
{

	public function __construct(int $amount)
	{
		parent::__construct(sprintf('Invalid amount:%s',$amount));
	}

}