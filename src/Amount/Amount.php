<?php
declare(strict_types=1);

namespace Insidesuki\ValueObject\Amount;

use DomainException;
use Insidesuki\ValueObject\Amount\Exception\InvalidAmountValue;

class Amount
{

	protected function __construct(
		public readonly int $amount
	)
	{
	}

	public static function createFromString(string $value): static
	{
		$valueInteger = (int)str_replace(['.', ','], '', $value);
		return static::create($valueInteger);

	}

	public static function createFromFloat(float $float):static{

		$multiplier = 10 ** (strlen(strstr((string)$float, '.')) - 1);
		$value = (int)round($float * $multiplier);

		return static::create($value);

	}


	public static function createFromSpanishFormat(string $amount):static{

		if(!str_contains($amount, ',')){
			$amount .= ',00';
		}

		return static::createFromString($amount);

	}



	public static function create(int $value,bool $negative = false): static
	{

		if((false === $negative) && $value < 0) {
			throw new InvalidAmountValue($value);
		}

		return new static($value);

	}


	public function change(int $newAmount): static
	{


		if ($newAmount !== $this->amount) {

			return static::create($newAmount);
		}

		return $this;

	}


	public function equals(Amount $amount): bool
	{
		return $amount->amount === $this->amount;
	}


	public function toFloat(int $divider = 100): float
	{

		return $this->amount / $divider;
	}

	public function toSpanishFormat(): string
	{

		return number_format($this->toFloat(), 2, ',', '.');

	}

	public function toEuro(): string
	{

		return $this->toSpanishFormat() . '€';

	}

	public function increment(int $increment):static{

		return $this->change($this->amount + $increment);

	}

	public function decrease(int $decreaseValue):static{

		if($decreaseValue > $this->amount){
			throw new DomainException(sprintf('Invalid decreaseValue:%s',$decreaseValue));
		}

		return $this->change($this->amount - $decreaseValue);

	}


}