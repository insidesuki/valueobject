<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Fundae\CategoriaProfesional\Exception;
use RuntimeException;

class EmptyCategoriaProfesionalException extends RuntimeException
{

	public function __construct()
	{
		parent::__construct('CategoriaProfesional is empty');
	}
}