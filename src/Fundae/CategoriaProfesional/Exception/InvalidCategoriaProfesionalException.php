<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Fundae\CategoriaProfesional\Exception;
use RuntimeException;

class InvalidCategoriaProfesionalException extends RuntimeException
{

	public function __construct(string $categoria)
	{
		parent::__construct(sprintf('Invalid CategoriaProfesional:%s',$categoria));
	}
}