<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Fundae\CategoriaProfesional;

use Insidesuki\ValueObject\Fundae\CategoriaProfesional\Exception\EmptyCategoriaProfesionalException;
use Insidesuki\ValueObject\Fundae\CategoriaProfesional\Exception\EmptyGrupoCotizacionException;
use Insidesuki\ValueObject\Fundae\CategoriaProfesional\Exception\InvalidCategoriaProfesionalException;
use Insidesuki\ValueObject\Fundae\CategoriaProfesional\Exception\InvalidGrupoCotizacionException;
use Insidesuki\ValueObject\String\StringValue;

class CategoriaProfesionalValue
{

	protected const VALID_CATEGORIES
		= [
			'1'  => 'Directivo',
			'2'  => 'Mando Intermedio',
			'3'  => 'Técnico',
			'4'  => 'Trabajador Cualificado',
			'5'  => 'Trabajador no Cualificado',

		];


	public readonly bool $isValid;
	protected string       $categoria;

	private function __construct(string $categoria, bool $isValid)
	{
		$this->categoria = $categoria;
		$this->isValid   = $isValid;
	}

	public static function createAllowEmpty(string $categoria): static
	{

		if(empty($categoria)) {
			return new static('', false);
		}
		return static::create($categoria);

	}

	public static function create(string $categoria): static
	{

		if(empty($categoria)) {
			throw new EmptyCategoriaProfesionalException();
		}
		$categoriaProfesional = self::clean($categoria);
		return new static($categoriaProfesional->__toString(), true);

	}

	private static function clean(string $nivel): StringValue
	{

		$categoriaString = new StringValue($nivel);
		$categoriaString->clean()->toTrim();
		$nivelEstudio = $categoriaString->__toString();
		if(!array_key_exists($nivelEstudio, self::VALID_CATEGORIES)) {

			throw new InvalidCategoriaProfesionalException($nivelEstudio);
		}
		return $categoriaString;


	}

	public function categoriaDesc(): string
	{
		return self::VALID_CATEGORIES[$this->categoria];
	}

	public function equals(CategoriaProfesionalValue $categoriaValue): bool
	{
		return $this->categoria === $categoriaValue->categoria();
	}

	public function categoria(): string
	{
		return $this->categoria;
	}

}