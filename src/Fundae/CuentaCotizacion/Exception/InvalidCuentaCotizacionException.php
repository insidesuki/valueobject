<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Fundae\CuentaCotizacion\Exception;
use RuntimeException;

class InvalidCuentaCotizacionException extends RuntimeException
{

	public function __construct(string $grupoCotizacion)
	{
		parent::__construct(sprintf('Invalid CuentaCotizacion:%s',$grupoCotizacion));
	}
}