<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Fundae\CuentaCotizacion\Exception;
use RuntimeException;

class EmptyCuentaCotizacionException extends RuntimeException
{

	public function __construct()
	{
		parent::__construct('CuentaCotizacion is empty');
	}
}