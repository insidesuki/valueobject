<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Fundae\CuentaCotizacion;

use Insidesuki\ValueObject\Fundae\CuentaCotizacion\Exception\EmptyCuentaCotizacionException;
use Insidesuki\ValueObject\Fundae\CuentaCotizacion\Exception\InvalidCuentaCotizacionException;
use Insidesuki\ValueObject\String\StringValue;

class CuentaCotizacionValue
{

	protected const VALID_LENGTHS = [9,11];


	public readonly bool $isValid;
	protected string       $cuentaCotizacion;

	private function __construct(string $cuenta, bool $isValid)
	{
		$this->cuentaCotizacion = $cuenta;
		$this->isValid          = $isValid;
	}

	public static function createAllowEmpty(string $cuenta): static
	{

		if(empty($cuenta)) {
			return new static('', false);
		}
		return static::create($cuenta);

	}

	public static function create(string $cuenta): static
	{

		if(empty($cuenta)) {
			throw new EmptyCuentaCotizacionException();
		}
		$cuentaCotizacion = self::clean($cuenta);
		return new static($cuentaCotizacion->string(), true);

	}

	private static function clean(string $cuenta): StringValue
	{

		$cuentaString = new StringValue($cuenta);
		$cuentaString->clean()->toTrim()->onlyNumbers();
		$grupoCotizacion = $cuentaString->string();

		if(!in_array($cuentaString->length(),self::VALID_LENGTHS)) {

			throw new InvalidCuentaCotizacionException($grupoCotizacion);
		}
		return $cuentaString;


	}


	public function equals(CuentaCotizacionValue $grupoCotizacionValue): bool
	{
		return $this->cuentaCotizacion === $grupoCotizacionValue->cuentaCotizacion();
	}

	public function cuentaCotizacion(): string
	{
		return $this->cuentaCotizacion;
	}

}