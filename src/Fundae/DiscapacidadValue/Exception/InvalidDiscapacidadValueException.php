<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Fundae\DiscapacidadValue\Exception;
use RuntimeException;

class InvalidDiscapacidadValueException extends RuntimeException
{

	public function __construct(string $message)
	{
		parent::__construct(sprintf('Invalid Value %s for Discapacidad',$message));
	}
}