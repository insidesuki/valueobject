<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Fundae\DiscapacidadValue\Exception;
use RuntimeException;

class EmptyDiscapacidadException extends RuntimeException
{
	public function __construct()
	{
		parent::__construct('Discapacidad value cant be empty!!!');
	}
}