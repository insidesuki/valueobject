<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Fundae\DiscapacidadValue;

use Insidesuki\ValueObject\Fundae\DiscapacidadValue\Exception\EmptyDiscapacidadException;
use Insidesuki\ValueObject\Fundae\DiscapacidadValue\Exception\InvalidDiscapacidadValueException;
use Insidesuki\ValueObject\String\StringValue;

class DiscapacidadValue
{

	private const ALLOWED_VALUES = ['0', '1'];
	public readonly bool $isValid;
	protected string     $discapacidad;

	private function __construct(string $discapacidad, bool $isValid)
	{
		$this->discapacidad = $discapacidad;
		$this->isValid      = $isValid;
	}

	public static function createAllowEmpty(string $discapacidad): static
	{

		if("" === $discapacidad) {
			return new static($discapacidad, false);
		}
		return static::create($discapacidad);

	}

	public static function create(string $discapacidad): static
	{

		if("" === $discapacidad) {
			throw new EmptyDiscapacidadException();
		}
		$discapacidadString = new StringValue($discapacidad);
		$discapacidadString->toTrim();
		$discapacidadValue = $discapacidadString->string();
		if(!in_array($discapacidadValue, self::ALLOWED_VALUES)) {
			throw new InvalidDiscapacidadValueException($discapacidad);
		}
		return new static($discapacidadValue, true);


	}

	public function equals(DiscapacidadValue $discapacidadValue):bool{
		return $this->discapacidad === $discapacidadValue->discapacidad();
	}

	public function discapacidad(): string
	{
		return $this->discapacidad;
	}




}