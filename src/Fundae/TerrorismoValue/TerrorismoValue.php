<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Fundae\TerrorismoValue;

use Insidesuki\ValueObject\Fundae\TerrorismoValue\Exception\EmptyTerrorismoException;
use Insidesuki\ValueObject\Fundae\TerrorismoValue\Exception\InvalidTerrorismoValueException;
use Insidesuki\ValueObject\String\StringValue;

class TerrorismoValue
{

	private const ALLOWED_VALUES = ['0', '1'];
	public readonly bool $isValid;
	protected string     $terrorismo;

	private function __construct(string $terrorismo, bool $isValid)
	{
		$this->terrorismo = $terrorismo;
		$this->isValid    = $isValid;
	}

	public static function createAllowEmpty(string $terrorismo): static
	{

		if("" === $terrorismo) {
			return new static($terrorismo, false);
		}
		return static::create($terrorismo);

	}

	public static function create(string $terrorismo): static
	{

		if("" === $terrorismo) {
			throw new EmptyTerrorismoException();
		}
		$terrorismoString = new StringValue($terrorismo);
		$terrorismoString->toTrim();
		$terrorismoValue = $terrorismoString->string();

		if(!in_array($terrorismoValue, self::ALLOWED_VALUES)) {
			throw new InvalidTerrorismoValueException($terrorismoValue);
		}
		return new static($terrorismoValue, true);


	}

	public function equals(TerrorismoValue $terrorismoValue):bool{
		return $this->terrorismo === $terrorismoValue->terrorismo();
	}

	public function terrorismo(): string
	{
		return $this->terrorismo;
	}




}