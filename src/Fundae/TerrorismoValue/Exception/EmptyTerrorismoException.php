<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Fundae\TerrorismoValue\Exception;
use RuntimeException;

class EmptyTerrorismoException extends RuntimeException
{
	public function __construct()
	{
		parent::__construct('Terrorismo value cant be empty!!!');
	}
}