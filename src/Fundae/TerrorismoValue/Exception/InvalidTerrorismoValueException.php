<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Fundae\TerrorismoValue\Exception;
use RuntimeException;

class InvalidTerrorismoValueException extends RuntimeException
{

	public function __construct(string $message)
	{
		parent::__construct(sprintf('Invalid Value %s for Terrorismo',$message));
	}
}