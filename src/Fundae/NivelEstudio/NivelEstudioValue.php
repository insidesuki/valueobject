<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Fundae\NivelEstudio;

use Insidesuki\ValueObject\Fundae\NivelEstudio\Exception\EmptyNivelEstudioException;
use Insidesuki\ValueObject\Fundae\NivelEstudio\Exception\InvalidNivelEstudioException;
use Insidesuki\ValueObject\String\StringValue;

class NivelEstudioValue
{

	protected const NIVELES_ACEPTADOS
		= [
			'1'  => 'Menos que primaria',
			'2'  => 'Educación primaria',
			'3'  => 'Primera etapa de educacion secundaria (1 y 2, ciclo de la ESO,EGB,Graduado Escolar,Certificados de profesionalidad 1,2)',
			'4'  => 'Segunda etapa de educación secundaria (Bachillerato, FP de grado medio, BUP, FPI y FPII)',
			'5'  => 'Educación postsecundaria no superior (Certificados de Profesionalidad de nivel 3)',
			'6'  => 'Técnico Superior/FP grado superior y equivalentes',
			'7'  => 'E. universitarios 1º ciclo (Diplomatura-Grados)',
			'8'  => 'E. Universitarios 2º ciclo (Licenciatura)',
			'9'  => 'E. Universitarios 3º ciclo (Doctorado)',
			'10' => 'Otras titulaciones',
		];


	public readonly bool $isValid;
	protected string       $nivelEstudio;

	private function __construct(string $nivelEstudio, bool $isValid)
	{
		$this->nivelEstudio = $nivelEstudio;
		$this->isValid      = $isValid;
	}

	public static function createAllowEmpty(string $nivel): static
	{

		if(empty($nivel)) {
			return new static('', false);
		}
		return static::create($nivel);

	}

	public static function create(string $nivel): static
	{

		if(empty($nivel)) {
			throw new EmptyNivelEstudioException();
		}
		$nivelEstudio = self::clean($nivel);
		return new static($nivelEstudio->__toString(), true);

	}

	private static function clean(string $nivel): StringValue
	{

		$nivelString = new StringValue($nivel);
		$nivelString->clean()->toTrim();
		$nivelEstudio = $nivelString->__toString();
		if(!array_key_exists($nivelEstudio, self::NIVELES_ACEPTADOS)) {

			throw new InvalidNivelEstudioException($nivelEstudio);
		}
		return $nivelString;


	}

	public function nivelEstudioDesc(): string
	{
		return self::NIVELES_ACEPTADOS[$this->nivelEstudio];
	}

	public function equals(NivelEstudioValue $nivelEstudioValue): bool
	{
		return $this->nivelEstudio === $nivelEstudioValue->nivelEstudio();
	}

	public function nivelEstudio(): string
	{
		return $this->nivelEstudio;
	}

}