<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Fundae\NivelEstudio\Exception;
use RuntimeException;

class EmptyNivelEstudioException extends RuntimeException
{

	public function __construct()
	{
		parent::__construct('ivelEstudio is empty');
	}
}