<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Fundae\NivelEstudio\Exception;
use RuntimeException;

class InvalidNivelEstudioException extends RuntimeException
{

	public function __construct(string $categoria)
	{
		parent::__construct(sprintf('Invalid NivelEstudio:%s',$categoria));
	}
}