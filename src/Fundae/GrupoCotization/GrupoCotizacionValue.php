<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Fundae\GrupoCotization;

use Insidesuki\ValueObject\Fundae\GrupoCotization\Exception\EmptyGrupoCotizacionException;
use Insidesuki\ValueObject\Fundae\GrupoCotization\Exception\InvalidGrupoCotizacionException;
use Insidesuki\ValueObject\String\StringValue;

class GrupoCotizacionValue
{

	protected const VALID_GROUPS
		= [
			'1'  => 'Ingenieros y Licenciados',
			'2'  => 'Ingenieros técnicos, Peritos y Ayudantes titulados',
			'3'  => 'Jefes administrativos y de taller',
			'4'  => 'Ayudantes no titulados',
			'5'  => 'Oficiales administrativos',
			'6'  => 'Subalternos',
			'7'  => 'Auxiliares administrativos',
			'8'  => 'Oficiales de primera y segunda',
			'9'  => 'Oficiales de tercera y especialistas',
			'10' => 'Trabajadores mayores de 18 años no cualificados',
			'11' => 'Trabajadores menores de dieciocho años'
		];


	public readonly bool $isValid;
	protected string       $grupoCotizacion;

	private function __construct(string $grupo, bool $isValid)
	{
		$this->grupoCotizacion = $grupo;
		$this->isValid         = $isValid;
	}

	public static function createAllowEmpty(string $grupo): static
	{

		if(empty($grupo)) {
			return new static('', false);
		}
		return static::create($grupo);

	}

	public static function create(string $grupo): static
	{

		if(empty($grupo)) {
			throw new EmptyGrupoCotizacionException();
		}
		$grupoCotizacion = self::clean($grupo);
		return new static($grupoCotizacion->__toString(), true);

	}

	private static function clean(string $grupo): StringValue
	{

		$grupoString = new StringValue($grupo);
		$grupoString->clean()->toTrim();
		$grupoCotizacion = $grupoString->__toString();
		if(!array_key_exists($grupoCotizacion, self::VALID_GROUPS)) {

			throw new InvalidGrupoCotizacionException($grupoCotizacion);
		}
		return $grupoString;


	}

	public function grupoCotizacionDesc(): string
	{
		return self::VALID_GROUPS[$this->grupoCotizacion];
	}

	public function equals(GrupoCotizacionValue $grupoCotizacionValue): bool
	{
		return $this->grupoCotizacion === $grupoCotizacionValue->grupoCotizacion();
	}

	public function grupoCotizacion(): string
	{
		return $this->grupoCotizacion;
	}

}