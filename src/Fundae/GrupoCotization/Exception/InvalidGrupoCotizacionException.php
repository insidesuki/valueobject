<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Fundae\GrupoCotization\Exception;
use RuntimeException;

class InvalidGrupoCotizacionException extends RuntimeException
{

	public function __construct(string $grupoCotizacion)
	{
		parent::__construct(sprintf('Invalid GrupoCotizacion:%s',$grupoCotizacion));
	}
}