<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Fundae\GrupoCotization\Exception;
use RuntimeException;

class EmptyGrupoCotizacionException extends RuntimeException
{

	public function __construct()
	{
		parent::__construct('GrupoCotizacion is empty');
	}
}