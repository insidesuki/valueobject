<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Fundae\ViolenciaValue\Exception;
use RuntimeException;

class EmptyViolenciaException extends RuntimeException
{
	public function __construct()
	{
		parent::__construct('Violencia value cant be empty!!!');
	}
}