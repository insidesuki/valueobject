<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Fundae\ViolenciaValue\Exception;

use Insidesuki\ValueObject\Fundae\DiscapacidadValue\Exception\EmptyDiscapacidadException;
use Insidesuki\ValueObject\Fundae\DiscapacidadValue\Exception\InvalidDiscapacidadValueException;
use Insidesuki\ValueObject\String\StringValue;

class ViolenciaValue
{

	private const ALLOWED_VALUES = ['0', '1'];
	public readonly bool $isValid;
	protected string     $violencia;

	private function __construct(string $violencia, bool $isValid)
	{
		$this->violencia = $violencia;
		$this->isValid   = $isValid;
	}

	public static function createAllowEmpty(string $violencia): static
	{

		if("" === $violencia) {
			return new static($violencia, false);
		}
		return static::create($violencia);

	}

	public static function create(string $violencia): static
	{

		if("" === $violencia) {
			throw new EmptyViolenciaException();
		}
		$violenciaString = new StringValue($violencia);
		$violenciaString->toTrim();
		$violenciaValue = $violenciaString->string();

		if(!in_array($violenciaValue, self::ALLOWED_VALUES)) {
			throw new InvalidViolenciaValueException($violencia);
		}
		return new static($violenciaValue, true);


	}

	public function equals(ViolenciaValue $violenciaValue):bool{
		return $this->violencia === $violenciaValue->violencia();
	}

	public function violencia(): string
	{
		return $this->violencia;
	}




}