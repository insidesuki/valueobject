<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Fundae\ViolenciaValue\Exception;
use RuntimeException;

class InvalidViolenciaValueException extends RuntimeException
{

	public function __construct(string $message)
	{
		parent::__construct(sprintf('Invalid Value %s for Violencia',$message));
	}
}