<?php
declare(strict_types=1);

namespace Insidesuki\ValueObject\Dates;

use DateTime;
use DateTimeZone;
use Insidesuki\ValueObject\Dates\Exception\InvalidDateException;

class DateValue
{
	private const DATE_FORMAT = 'd-m-Y';
	private const SPANISH_MONTHS = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
	private const SPANISH_DAYS = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado'];
	public readonly string $dateValue;
	public readonly bool $isValid;
	protected ?DateTime $date;
	private string $timeValue;

	private function __construct(?DateTime $date, bool $valid)
	{
		$this->date = $date;
		$this->dateValue = (null === $date) ? '' : $date->format(self::DATE_FORMAT);
		$this->timeValue = (null === $date) ? '' : $date->format('H:i');
		$this->isValid = $valid;
	}

	public function format(string $format): string
	{

		return $this->getDate()?->format($format);

	}

	public static function now(string $timezone = 'Europe/Madrid', string $format = self::DATE_FORMAT): static
	{


		$now = new DateTime('', new DateTimeZone($timezone));
		return new static($now, true);
	}

	public function age(): string
	{
		if (null === $this->date) {
			return 'undefined';

		}
		$today = new DateTime();
		return $today->diff($this->date)->format('%Y');

	}

	public function change(string $newFecha): self
	{

		if (!$this->equalsByValue($newFecha)) {
			return self::createAllowEmpty($newFecha);
		}
		return $this;

	}

	public function equalsByValue(string $fecha): bool
	{
		return $this->dateValue === $fecha;
	}

	public static function createAllowEmpty(string $date): static
	{

		if (empty($date)) {
			return new static(null, false);
		}
		return static::create($date);

	}

    public static function createIfNotWeekend(string $date, string $format = self::DATE_FORMAT): ?static
    {
        $newDate = DateTime::createFromFormat($format, $date);
        if (in_array($newDate->format('N'), [6, 7])) {
            return null;
        }

        return static::create($date, $format);
    }


    public static function create(string $date, string $format = self::DATE_FORMAT): static
	{

		$newDate = DateTime::createFromFormat($format, $date);
		if (!$newDate) {
			throw InvalidDateException::hasInvalid($date);
		}
		if ($newDate->format($format) !== $date) {
			throw InvalidDateException::hasIncorrectFormat($date, $format);
		}
		return new static($newDate, true);

	}

	public function increment(string $interval): static
	{

		return $this->modify('+' . $interval);

	}

	private function modify(string $interval): static
	{
		$newDateTime = clone($this->date);
		$newDateTime->modify($interval);
		return new static($newDateTime, true);
	}

	public function decrement(string $interval): static
	{

		return $this->modify('-' . $interval);

	}

    public function isWeekend(): bool
    {
        return in_array($this->date->format('N'), [6, 7]);
    }

	public function equals(DateValue $dateValue): bool
	{
		return $this->dateValue === $dateValue->dateValue;
	}

	public function getDate(): ?DateTime
	{
		return $this->date;
	}


	/**
	 * @param bool $withMonth
	 * @param bool $withYear
	 * @return string
	 */
	public function getSpanishStringDate(bool $withMonth = true, bool $withYear = true): string
	{


		$day = $this->date->format('j');
		$w = $this->date->format('w');
		$month = $this->date->format('m');
		$year = $this->date->format('Y');

		$content = $day;
		if ($withMonth) {

			$content .= ' de ' . self::SPANISH_MONTHS[$month - 1];
		}

		if ($withYear) {
			$content .= ' de ' . $year;
		}

		return $content;

	}


	public function month(): string
	{

		$month = $this->date->format('m');
		return self::SPANISH_MONTHS[$month - 1];
	}


}