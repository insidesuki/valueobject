<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\Dates\Exception;
use RuntimeException;

class InvalidDateException extends RuntimeException
{

	private function __construct(string $message){

		parent::__construct(sprintf('Invalid DateValue:%s',$message));

	}

	public static function hasInvalid(string $date){
		throw new self(sprintf(' %s',$date));
	}

	public static function hasIncorrectFormat(string $date,string $format):static{

		throw new static(sprintf('Incorrect format:%s, to %s',$format,$date));

	}


}