<?php

declare(strict_types=1);

namespace Insidesuki\ValueObject\Dates;

use DateTime;

/**
 * Get Available dates in certain date
 * @authors: sys@insideapps.net,david656574411@gmail.com
 * Get available dates in on month and year
 */
class AvailableDates
{


    private function __construct(
        public readonly DateTime $baseDate,
        public readonly ?ExcludedDates $excludedDates,
        public readonly array $excludedDays,
        public readonly array $dateList
    ) {
    }

    public static function createWithoutWeekends(
        DateTime $baseDate,
        ?ExcludedDates $excludedDates = null
    ): static {
        return self::create(baseDate: $baseDate, excludedDays: [6, 7], excludedDates: $excludedDates);
    }

    private static function create(
        DateTime $baseDate,
        array $excludedDays,
        ?ExcludedDates $excludedDates = null
    ): static {
        $yearAndMonth = $baseDate->format('Y') . '-' . $baseDate->format('m');

        $firstDay = new DateTime(date('Y-m-01', strtotime($yearAndMonth)));
        $lastDay = new DateTime(date('Y-m-t', strtotime($yearAndMonth)));

        $availableDates = [];

        while ($firstDay <= $lastDay) {
            // check if day is not in excludedDates and excludedDays
            if (!in_array($firstDay->format("N"), $excludedDays)) {
                if (null !== $excludedDates) {
                    if (!in_array($firstDay->format('d-m-Y'), $excludedDates->inSpanishFormat())) {
                        $availableDates[] = DateValue::create($firstDay->format('d-m-Y'));
                    }
                } else {
                    $availableDates[] = DateValue::create($firstDay->format('d-m-Y'));
                }
            }
            // move to next day
            $firstDay->modify("+1 day");
        }


        return new static(
            baseDate: $baseDate,
            excludedDates: $excludedDates,
            excludedDays: $excludedDays,
            dateList: $availableDates
        );
    }

    public function inSpanishFormat(): array
    {
        return array_map(fn($obj) => $obj->format('d-m-Y'), $this->dateList);
    }

    public static function createWithoutSunday(
        DateTime $baseDate,
        ?ExcludedDates $excludedDates = null
    ): static {
        return self::create(baseDate: $baseDate, excludedDays: [7], excludedDates: $excludedDates);
    }

    public static function createWithoutSaturday(
        DateTime $baseDate,
        ?ExcludedDates $excludedDates = null
    ): static {
        return self::create(baseDate: $baseDate, excludedDays: [6], excludedDates: $excludedDates);
    }


}