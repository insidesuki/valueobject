<?php

declare(strict_types=1);

namespace Insidesuki\ValueObject\Dates;

class ExcludedDates
{

    private array $dates;

    public function __construct(array $excludedDates = [])
    {
        array_map([$this, 'add'], $excludedDates);
    }

    public function inSpanishFormat(): array
    {
        return (!empty($this->dates)) ? array_map(fn($obj) => $obj->format('d-m-Y'), $this->dates) : [];
    }

    public function getAll(): array
    {
        return $this->dates;
    }

    public function add(\DateTime $date): void
    {
        $this->dates[] = $date;
    }

    public function __toArray(): array
    {
        return $this->dates;
    }


}