<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\String;
class StringValue
{

	protected string $string;

	private int $length;

	public function __construct(string $string)
	{
		$this->string = $string;

	}

	public function removeSpaces(): static
	{
		$this->string = str_replace(' ', '', trim($this->string));
		return $this;
	}

	public function toTrim(): static
	{
		$this->string = trim($this->string);
		return $this;
	}

	public function clean():static
	{
		// clean up,
		$clean = preg_replace("/[^a-zA-Z0-9]+/", "", $this->string);
		$this->string = $clean;
		return $this;
	}

	public function onlyNumbers(): static
	{
		$numbers      = preg_replace("/[^0-9,-]+/", "", $this->string);
		$this->string = $numbers;
		return $this;
	}

	public function onlyLetters(): static
	{

		$text         = preg_replace("/[^a-zA-Z ]+/", "", $this->string);
		$this->string = $text;
		return $this;

	}


	/**
	 * Check if a string starts with range of values
	 * @param array $starts
	 * @return bool
	 */
	public function startsWith(array $starts): bool
	{
		$firstNumber = $this->string[0];
		return in_array($firstNumber, $starts, true);
	}

	public function toUpper(): static
	{
		$upper = strtoupper($this->string);
		$this->string = $upper;
		return $this;
	}


	public function length(): int
	{
		return $this->length = strlen($this->string);
	}

	public function validLength(int $position): bool
	{
		$this->length();
		return $this->length === $position;
	}


	public function isFilled(): bool
	{
		return "" !== $this->string;
	}

	public function isOnlyLetters():bool
	{
		if(!preg_match("#^[a-zA-Z ]+$#",$this->string)){
			return false;
		}
		return true;
	}

	public function __toString(): string
	{
		return $this->string;
	}


	public function string(): string
	{
		return $this->string;
	}


}