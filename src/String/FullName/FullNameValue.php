<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\String\FullName;

use Insidesuki\ValueObject\String\FullName\Exception\InvalidDataFullName;
use Insidesuki\ValueObject\String\StringValue;

/**
 * Simple Value Object to FullName attributes
 *
 */
class FullNameValue
{

	public readonly StringValue $nameValue;
	public readonly StringValue $lastnameValue;
	public readonly StringValue $lastname2Value;
	protected string      $name;
	protected string      $lastname1 = '';
	protected ?string      $lastname2;

	private function __construct(StringValue $nameValue, StringValue $lastnameValue, StringValue $lastname2Value)
	{
		$this->nameValue      = $nameValue;
		$this->lastnameValue  = $lastnameValue;
		$this->lastname2Value = $lastname2Value;
		$this->name           = $this->nameValue->__toString();
		$this->lastname1      = $this->lastnameValue->__toString();
		$this->lastname2      = $this->lastname2Value->__toString();
	}


	public function fullName(): string
	{

		if(empty($this->lastname2)) {
			return $this->name . ' ' . $this->lastname1;
		}
		return $this->name . ' ' . $this->lastname1.' '.$this->lastname2;

	}


	public function fullNameByLastname(): string
	{

		if(empty($this->lastname2)) {
			return $this->lastname1 . ', ' . $this->name;
		}
		return $this->lastname1 . ' ' . $this->lastname2 . ', ' . $this->name;

	}

	public function change(string $name, string $lastname, string $lastname2 = ''): static
	{

		if($this->name !== $name || $this->lastname1 !== $lastname || $this->lastname2 !== $lastname2) {

			return static::create($name, $lastname, $lastname2);

		}
		return $this;


	}

	public static function create(string $name, string $lastname1, string $lastname2 = ''): static
	{

		$nameValue = new StringValue($name);
		$nameValue->toTrim();
		if(!$nameValue->isFilled()) {

			throw InvalidDataFullName::byName();

		}
		$lastnameValue = new StringValue($lastname1);
		$lastnameValue->toTrim();
		if(!$lastnameValue->isFilled()) {
			throw InvalidDataFullName::byLastname1();
		}
		$lastname2Value = new StringValue($lastname2);
		$lastname2Value->toTrim();
		return new static($nameValue, $lastnameValue, $lastname2Value);


	}

	public function equals(FullNameValue $fullNameValue): bool
	{

		return $this->name === $fullNameValue->name
			&& $this->lastname1 === $fullNameValue->lastname1
			&& $this->lastname2 === $fullNameValue->lastname2;

	}

	public function name(): string
	{
		return $this->name;
	}

	public function lastname1(): string
	{
		return $this->lastname1;
	}

	public function lastname2(): ?string
	{
		return $this->lastname2;
	}

}