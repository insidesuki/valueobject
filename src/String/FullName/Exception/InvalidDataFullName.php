<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\String\FullName\Exception;

use InvalidArgumentException;

class InvalidDataFullName extends InvalidArgumentException
{

	private function __construct(string $field){
		parent::__construct(sprintf('%s is required',$field));
	}

	public static function byName(){
		throw new self('Name');
	}

	public static function byLastname1(){
		throw new self('Lastname1');
	}
}