<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\String\Phone\Exception;

use RuntimeException;

class EmptyPhoneNumberException extends RuntimeException
{

	public function __construct()
	{
		parent::__construct('Empty phone number');
	}


}