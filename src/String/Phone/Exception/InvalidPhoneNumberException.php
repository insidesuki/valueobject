<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\String\Phone\Exception;

use RuntimeException;

class InvalidPhoneNumberException extends RuntimeException
{

	private function __construct(string $phoneNumber)
	{
		parent::__construct(sprintf('Invalid phone number:%s', $phoneNumber));
	}

	public static function byNumber(string $number): self
	{
		throw new self($number);
	}

}