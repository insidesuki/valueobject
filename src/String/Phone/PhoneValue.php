<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\String\Phone;
use Insidesuki\ValueObject\String\Phone\Exception\EmptyPhoneNumberException;
use Insidesuki\ValueObject\String\Phone\Exception\InvalidPhoneNumberException;
use Insidesuki\ValueObject\String\StringValue;

class PhoneValue
{


	public const  DEFAULT_LENGTH = 9;
	public const  SPANISH_CELL   = ['6', '7'];
	public const  SPANISH_PHONE  = ['9'];


	protected string $phoneNumber;

	public readonly bool $isValid;

	private function __construct(string $phoneNumber, bool $isValid)
	{
		$this->phoneNumber = $phoneNumber;
		$this->isValid     = $isValid;
	}


	private static function clean(string $number, int $length): StringValue
	{

		$phoneString        = new StringValue($number);
		$phoneStringCleaned = $phoneString->toTrim()->onlyNumbers();
		if(!$phoneStringCleaned->validLength($length)) {
			throw InvalidPhoneNumberException::byNumber($number);
		}
		return $phoneStringCleaned;

	}

	public static function create(string $number):static{

		if(empty($number)) {
			throw new EmptyPhoneNumberException();
		}

		$phoneString = self::clean($number, self::DEFAULT_LENGTH);
		$valids = array_merge(self::SPANISH_PHONE,self::SPANISH_CELL);
		if(!$phoneString->startsWith($valids)) {
			throw InvalidPhoneNumberException::byNumber($phoneString->__toString());
		}
		return new static($phoneString->string(), true);
	}

	public static function createAllowEmpty(string $number):static{

		if(empty($number)) {
			return new static($number, false);
		}

		return self::create($number);
	}

	public static function createPhone(string $number): static
	{

		if(empty($number)) {
			throw new EmptyPhoneNumberException();
		}
		$phoneString = self::clean($number, self::DEFAULT_LENGTH);
		if(!$phoneString->startsWith(self::SPANISH_PHONE)) {
			throw InvalidPhoneNumberException::byNumber($phoneString->__toString());
		}
		return new static($phoneString->__toString(), true);

	}


	public static function createPhoneAllowEmpty(string $number): static
	{

		if(empty($number)) {
			return new static($number, false);
		}
		return self::createPhone($number);

	}



	public static function createMovilPhone(string $number): static
	{

		if(empty($number)) {
			throw new EmptyPhoneNumberException();
		}
		$phoneString = self::clean($number, self::DEFAULT_LENGTH);
		if(!$phoneString->startsWith(self::SPANISH_CELL)) {
			throw InvalidPhoneNumberException::byNumber($phoneString->__toString());
		}
		return new static($phoneString->__toString(), true);

	}



	public function equals(PhoneValue $phoneValue): bool
	{
		return $this->phoneNumber === $phoneValue->phoneNumber();
	}


	public function phoneNumber(): string
	{
		return $this->phoneNumber;
	}


}