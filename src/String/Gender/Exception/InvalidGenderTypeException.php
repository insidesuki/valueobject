<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\String\Gender\Exception;
use InvalidArgumentException;

class InvalidGenderTypeException extends InvalidArgumentException
{

	public function __construct(string $gender)
	{
		parent::__construct(sprintf('Invalid value:%s for gender',$gender));
	}

}