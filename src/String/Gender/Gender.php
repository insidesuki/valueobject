<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\String\Gender;

use Insidesuki\ValueObject\String\Gender\Exception\InvalidGenderTypeException;
use UnhandledMatchError;

/**
 * simple Gender VO
 */
class Gender
{

	public readonly GenderType $genderType;

	protected string $gender;

	public readonly bool $isValid;

	private function __construct(GenderType $genderType, bool $valid)
	{
		$this->genderType = $genderType;
		$this->gender     = $this->genderType->value;
		$this->isValid      = $valid;
	}

	public function change(string $gender): static
	{

		$genderValue = trim($gender);
		if(false === $this->equalsByValue($genderValue)) {
			return static::create($genderValue);
		}
		return $this;

	}

	public function equalsByValue(string $value): bool
	{

		return $this->gender === $value;
	}

	public static function create(string $gender): static
	{

		$genderValue = trim($gender);
		try {

			return match ($genderValue) {

				'' => new static(GenderType::Undefined, false),
				'1' => new static(GenderType::Masculino, true),
				'2' => new static(GenderType::Femenino, true)

			};

		}
		catch (UnhandledMatchError) {

			throw new InvalidGenderTypeException($genderValue);

		}


	}

	public function equals(Gender $gender): bool
	{

		return $this->gender === $gender->gender;

	}

	public function gender(): string
	{
		return $this->gender;
	}





}