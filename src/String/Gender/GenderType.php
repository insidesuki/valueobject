<?php

namespace Insidesuki\ValueObject\String\Gender;
enum GenderType:string
{

	case Masculino = '1';
	case Femenino = '2';
	case Undefined = '';

}
