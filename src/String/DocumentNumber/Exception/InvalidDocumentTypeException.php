<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\String\DocumentNumber\Exception;
use RuntimeException;

class InvalidDocumentTypeException extends RuntimeException
{

	public function __construct(string $type,string $documentNumber)
	{
		parent::__construct(sprintf('Invalid DocumentNumber.Type: %s for %s', $type,$documentNumber));
	}

}