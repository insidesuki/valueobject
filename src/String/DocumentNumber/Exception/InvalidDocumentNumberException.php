<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\String\DocumentNumber\Exception;
use RuntimeException;

class InvalidDocumentNumberException extends RuntimeException
{
	private function __construct(string $numDocumento,string $type)
	{
		parent::__construct(sprintf('The DocumentNumber: %s, with type: %s has invalid',$numDocumento,$type));
	}


	public static function byNif(string $numDocumento):self{

		throw new self($numDocumento,'NIF');

	}

	public static function byCif(string $numDocumento):self{

		throw new self($numDocumento,'CIF');

	}

	public static function byNie(string $numDocumento):self{

		throw new self($numDocumento,'NIE');

	}

	public static function byNumDocument(string $numDocument):self{

		throw new self($numDocument,'PASSPORT');
	}

}