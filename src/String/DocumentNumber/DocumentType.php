<?php

namespace Insidesuki\ValueObject\String\DocumentNumber;
/**
 * FFTE values for dni
 */
enum DocumentType: string
{
	case nif = '10';
	case passport = '20';
	case nie = '60';
	case cif = '90';

}
