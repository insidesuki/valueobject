<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\String\DocumentNumber;
use Insidesuki\ValueObject\String\DocumentNumber\Exception\InvalidDocumentNumberException;
use Insidesuki\ValueObject\String\DocumentNumber\Exception\InvalidDocumentTypeException;
use Insidesuki\ValueObject\String\DocumentNumber\Helper\DocumentValidator;
use Insidesuki\ValueObject\String\StringValue;
use UnhandledMatchError;

/**
 * Document Number, dni, cif, nie, passport
 */
class DocumentNumber
{
	public readonly bool   $isValid;
	protected string       $numDocument;
	protected DocumentType $documentType;

	protected string $descDocumentType;

	protected string $type;

	private function __construct(string $numDocument, DocumentType $documentType, bool $valid = true)
	{
		$this->numDocument      = $numDocument;
		$this->documentType     = $documentType;
		$this->descDocumentType = $documentType->name;
		$this->type             = $documentType->value;
		$this->isValid          = $valid;
	}

	public static function create(string $numDocument, bool $validate = true): static
	{

		$numDocumentoCleaned = new StringValue($numDocument);
		$numDocString        = $numDocumentoCleaned->clean()->toTrim()->toUpper()->__toString();


		if(false === $validate) {
			return new static($numDocString, DocumentType::passport, false);
		}

		try{
			return match (true){
				DocumentValidator::isValidDni($numDocString) => new static($numDocString,DocumentType::nif),
				DocumentValidator::isValidNie($numDocString) => new static($numDocString,DocumentType::nie),
				DocumentValidator::isValidCif($numDocString) => new static($numDocString,DocumentType::cif)
			};
		}
		catch(UnhandledMatchError){
			throw InvalidDocumentNumberException::byNumDocument($numDocString);
		}

	}

	public static function createByType(string $type, string $numDocument): static
	{

		$numDocumentoString = new StringValue($numDocument);
		$numDocString       = $numDocumentoString->clean()->toTrim()->toUpper()->__toString();
		try {
			return match ($type) {

				'10' => static::createNif($numDocString),
				'20' => new static($numDocString, DocumentType::passport, false),
				'60' => static::createNie($numDocString),
				'90' => static::createCif($numDocString)

			};
		}
		catch (UnhandledMatchError) {

			throw new InvalidDocumentTypeException($type, $numDocString);

		}


	}


	private static function createNif(string $numDocument): static
	{
		if(false === DocumentValidator::isValidDni($numDocument)) {
			throw InvalidDocumentNumberException::byNif($numDocument);
		}
		return new static($numDocument, DocumentType::nif, true);

	}

	private static function createNie(string $numDocument): static
	{
		if(false === DocumentValidator::isValidNie($numDocument)) {
			throw InvalidDocumentNumberException::byNie($numDocument);
		}
		return new static($numDocument, DocumentType::nie, true);

	}

	private static function createCif(string $numDocument): static
	{
		if(false === DocumentValidator::isValidCif($numDocument)) {
			throw InvalidDocumentNumberException::byCif($numDocument);
		}
		return new static($numDocument, DocumentType::cif, true);

	}


	public function change(string $type,string $numDocument): static
	{

		$numDocumentoString = new StringValue($numDocument);
		$numDocString       = $numDocumentoString->clean()->toTrim()->toUpper()->__toString();
		if(false === $this->equalsByString($numDocString, $type)) {
			return static::createByType($type,$numDocString);
		}
		return $this;

	}

	private function equalsByString(string $numDocument, string $type): bool
	{


		return $this->numDocument === $numDocument && $this->type === $type;

	}


	public function equals(self $numDocument): bool
	{

		return ($this->numDocument === $numDocument->numDocument && $this->documentType === $numDocument->documentType);

	}

	public function numDocument(): string
	{
		return $this->numDocument;
	}

	public function documentType(): DocumentType
	{

		return $this->documentType;
	}

	public function descDocumentType(): string
	{
		return $this->descDocumentType ?? 'undefined';
	}

	public function type(): string
	{
		return $this->type;
	}


}