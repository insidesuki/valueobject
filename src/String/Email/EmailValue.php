<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\String\Email;

use Insidesuki\ValueObject\String\Email\Exception\InvalidEmailException;

class EmailValue
{

	public string $emailAddress;


	public readonly bool $isValid;

	private function __construct(string $emailAddress, bool $isValid)
	{
		$this->emailAddress = $emailAddress;
		$this->isValid      = $isValid;
	}

	public static function createAllowEmpty(string $emailAddress): static
	{

		if(empty($emailAddress)) {
			return new static($emailAddress, false);
		}
		return static::create($emailAddress);

	}

	public static function create(string $emailAddress): static
	{

		if(!filter_var(trim($emailAddress), FILTER_VALIDATE_EMAIL)) {
			throw new InvalidEmailException($emailAddress);
		}
		return new static($emailAddress, true);

	}

	public function equals(EmailValue $emailValue): bool
	{
		return $this->emailAddress === $emailValue->emailAddress;
	}






}