<?php
declare(strict_types = 1);

namespace Insidesuki\ValueObject\String\Email\Exception;
use RuntimeException;

class InvalidEmailException extends RuntimeException
{


	public function __construct(string $emailAddress)
	{
		parent::__construct(sprintf('Invalid emailAddress:%s',$emailAddress));
	}

}